# AQA instruction set VM

A virtual machine I wrote in javascript that executes AQA syntax assembly. 

Support for conditional branching and labeling.

AQA syntax : https://filestore.aqa.org.uk/resources/computing/AQA-75162-75172-ALI.PDF